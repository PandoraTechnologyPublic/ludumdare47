using TMPro;
using UnityEngine;

namespace Pandora.Ui
{
    public class LevelUiLayout : MonoBehaviour
    {
        [SerializeField] private TMP_Text _levelNameText;
        [SerializeField] private TMP_Text _hiScoreText;

        public void SetLevelName(string levelName)
        {
            _levelNameText.SetText(levelName);
        }

        public void SetLevelCompletion(bool hasLevelBeenCompleted)
        {
            if (hasLevelBeenCompleted)
            {
                _hiScoreText.SetText("Completed");
            }
            else
            {
                _hiScoreText.SetText("Not completed");
            }
        }

        public void SetBestScore(int bestScore)
        {
            if (bestScore > 0)
            {
                _hiScoreText.SetText($"Best: {bestScore}");
            }
            else
            {
                _hiScoreText.SetText("Not completed");
            }
        }
    }
}