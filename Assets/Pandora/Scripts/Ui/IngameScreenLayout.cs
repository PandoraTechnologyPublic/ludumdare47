using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora.Ui
{
    public class IngameScreenLayout : MonoBehaviour
    {
        [SerializeField] private TMP_Text _stepCountText;
        [SerializeField] private TMP_Text _runNumberText;
        [SerializeField] private Button _undoButton;
        [SerializeField] private Button _restartButton;

        private GameManager _gameManager;

        public void InitialiseDependencies(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        private void Start()
        {
            _restartButton.onClick.AddListener(OnRestartButton);
            _undoButton.onClick.AddListener(OnUndoButton);
            HideRunNumber();
        }

        private void Update()
        {
            string levelName = _gameManager.GetCurrentLevelName();
            string score = $"{levelName} - Steps:{_gameManager.GetTimeStep()}";

            int bestScore = _gameManager.GetCurrentLevelBestScore();

            if (bestScore > 0)
            {
                score += $"[Best: {bestScore}]";
            }

            _stepCountText.SetText(score);
        }

        private void OnRestartButton()
        {
            _gameManager.RestartLevel();
        }

        private void OnUndoButton()
        {
            _gameManager.Undo();
            #if !UNITY_EDITOR
            _undoButton.interactable = false;
            #endif
        }

        public void SetRunNumber(int timeStep)
        {
            _runNumberText.SetText($"{timeStep}");
            _runNumberText.gameObject.SetActive(true);
        }

        public void HideRunNumber()
        {
            _runNumberText.gameObject.SetActive(false);
        }

        public void SetMessage(string message)
        {
            _runNumberText.SetText(message);
            _runNumberText.gameObject.SetActive(true);
        }

        public void ResetUndo()
        {
            _undoButton.interactable = true;
        }
    }
}