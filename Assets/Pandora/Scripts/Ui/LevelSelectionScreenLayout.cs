using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora.Ui
{
    public class LevelSelectionScreenLayout : MonoBehaviour
    {
        [SerializeField] private LevelUiLayout _levelUiLayoutPrefab;
        [SerializeField] private RectTransform _levelPanel;

        private LevelManager _levelManager;

        private readonly Dictionary<string, LevelUiLayout> _levelUiLayouts = new Dictionary<string, LevelUiLayout>();

        public void InitialiseDependencies(LevelManager levelManager)
        {
            _levelManager = levelManager;

            List<LevelComponent> levelComponents = _levelManager.GetLevels();

            foreach (LevelComponent levelComponent in levelComponents)
            {
                LevelUiLayout levelUiLayout = GameObject.Instantiate(_levelUiLayoutPrefab, _levelPanel);
                levelUiLayout.SetLevelName(levelComponent.name);
                levelUiLayout.GetComponent<Button>().onClick.AddListener(() => OnLevelSelected(levelComponent));
                _levelUiLayouts.Add(levelComponent.name, levelUiLayout);
            }

            UpdateData();
        }

        private void OnLevelSelected(LevelComponent levelComponent)
        {
            _levelManager.SelectLevel(levelComponent);
        }

        public void UpdateData()
        {
            List<LevelComponent> levelComponents = _levelManager.GetLevels();

            foreach (LevelComponent levelComponent in levelComponents)
            {
                int bestScore = PlayerPrefs.GetInt(levelComponent.name, 0);
                LevelUiLayout levelUiLayout = _levelUiLayouts[levelComponent.name];
                levelUiLayout.SetLevelName(levelComponent.name);
                levelUiLayout.SetBestScore(bestScore);
            }
        }
    }
}