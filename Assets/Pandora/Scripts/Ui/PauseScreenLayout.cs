using System;
using Pandora.States;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora.Ui
{
    public class PauseScreenLayout : MonoBehaviour
    {
        [SerializeField] private Button _continueButton;
        [SerializeField] private Button _levelSelectionButton;
        [SerializeField] private Button _exitButton;

        private GameManager _gameManager;

        public void InitialiseDependencies(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        private void Start()
        {
            _continueButton.onClick.AddListener(OnContinueButton);
            _levelSelectionButton.onClick.AddListener(OnLevelSelectionButton);
            _exitButton.onClick.AddListener(OnExitButton);
        }

        private void OnContinueButton()
        {
            _gameManager.ChangeState<IngameState>(_gameManager);
        }

        private void OnLevelSelectionButton()
        {
            _gameManager.ChangeState<LevelSelectorState>(_gameManager);
        }

        private void OnExitButton()
        {
            #if UNITY_EDITOR
                EditorApplication.isPlaying = false;
            #endif

            Application.Quit();
        }
    }
}