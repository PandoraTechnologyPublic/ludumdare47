using System;
using UnityEngine;

namespace Pandora
{
    public class CameraManager : MonoBehaviour
    {
        private static readonly float CAMERA_SPEED = 5.0f;

        private Transform _mainCameraTargetTransform;
        private Transform _targetTransform;

        private void Start()
        {
            _mainCameraTargetTransform = Camera.main.transform.parent;
        }

        private void Update()
        {
            if (_targetTransform != null)
            {
                float distanceToTarget = Vector3.Distance(_mainCameraTargetTransform.position, _targetTransform.position);

                if (distanceToTarget > 0)
                {
                    float movementRatio = (CAMERA_SPEED * Time.deltaTime) / distanceToTarget;
                    Vector3 nextPosition = Vector3.Lerp(_mainCameraTargetTransform.position, _targetTransform.position, movementRatio);
                    nextPosition.y = 0;
                    _mainCameraTargetTransform.position = nextPosition;
                }
            }

            else
            {
                _mainCameraTargetTransform.position = Vector3.zero;
            }
        }

        public void SetTarget(Transform targetTransform)
        {
            _targetTransform = targetTransform;
        }
    }
}