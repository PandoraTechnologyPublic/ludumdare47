﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Pandora
{
    public class PlayerController : MonoBehaviour
    {
        private InputAction _playerMoveInputAction;

        private PlayerInput _playerInput;

        private GameManager _gameManager;

        public void InitialiseDependencies(GameManager gameManager)
        {
            _gameManager = gameManager;
        }
        private void Start()
        {
            _playerInput = GetComponent<PlayerInput>();
            _playerMoveInputAction = _playerInput.actions["Player/Move"];

            _playerMoveInputAction.performed += MovePerformed;
        }

        private void MovePerformed(InputAction.CallbackContext obj)
        {
            Vector2 playerMoveValue = _playerMoveInputAction.ReadValue<Vector2>();

            _gameManager.MoveTriggered(playerMoveValue);
        }
    }
}
