using System.Collections.Generic;
using UnityEngine;

namespace Pandora
{
    public class LevelManager : MonoBehaviour
    {
        [SerializeField] private List<LevelComponent> _levelPrefabs;

        private GameManager _gameManager;

        public void InitialiseDependencies(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        public LevelComponent GetLevelPrefab()
        {
            return _levelPrefabs[0];
        }

        public List<LevelComponent> GetLevels()
        {
            return _levelPrefabs;
        }

        public void SelectLevel(LevelComponent levelComponent)
        {
            _gameManager.LoadLevel(levelComponent);
        }
    }
}