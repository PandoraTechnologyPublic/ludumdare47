using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Pandora.LevelBlockComponents;
using Pandora.States;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.InputSystem;

namespace Pandora
{
    public class GameManager : MonoBehaviour
    {
        private static readonly float MIN_TIME_BETWEEN_INPUT = 0.1f;

        [SerializeField] private GameObject _titleScreenBullshit;
        [SerializeField] private PlayerController _playerControllerPrefab;
        [SerializeField] private MovementComponent _playerPrefab;
        [SerializeField] private AudioSource _playerMoveSound;

        private int _runIndex = 0;
        private int _timeStep = 0;

        private readonly List<List<Vector2>> _runInputs = new List<List<Vector2>>();
        private readonly List<MovementComponent> _players = new List<MovementComponent>();

        private Coroutine _previousRunStartPlaysCoroutine;

        private readonly List<CanonComponent> _canons = new List<CanonComponent>();
        private readonly List<Vector3> _wallTiles = new List<Vector3>();
        private LevelComponent _currentLevel;
        private LevelComponent _currentLevelPrefab;

        private readonly StateMachine<GameManager> _gameStateMachine = new StateMachine<GameManager>();

        private LevelManager _levelManager;
        private GuiManager _guiManager;

        private float _timeSinceLastInput;

        private readonly List<Vector2> _nextFrames = new List<Vector2>();

        private void Start()
        {
            _playerMoveSound.gameObject.SetActive(false);

            PlayerController playerController = GameObject.Instantiate(_playerControllerPrefab);
            playerController.InitialiseDependencies(this);

            _levelManager = GetComponentInChildren<LevelManager>();
            _levelManager.InitialiseDependencies(this);

            _guiManager = GameObject.FindObjectOfType<GuiManager>();
            _guiManager.InitialiseDependencies(this, _levelManager);

            PlayerInput playerInput = playerController.GetComponent<PlayerInput>();

            EngagementScreenState engagementScreenState = new EngagementScreenState();
            engagementScreenState.InitialiseDependencies(_guiManager, playerInput);
            _gameStateMachine.RegisterState(engagementScreenState);

            LevelSelectorState levelSelectorState = new LevelSelectorState();
            levelSelectorState.InitialiseDependencies(_guiManager);
            _gameStateMachine.RegisterState(levelSelectorState);

            IngameState ingameState = new IngameState();
            ingameState.InitialiseDependencies(_guiManager, playerInput);
            _gameStateMachine.RegisterState(ingameState);

            PauseMenuState pauseMenuState = new PauseMenuState();
            pauseMenuState.InitialiseDependencies(_guiManager, playerInput);
            _gameStateMachine.RegisterState(pauseMenuState);

            VictoryState victoryState = new VictoryState();
            victoryState.InitialiseDependencies(_guiManager, playerInput);
            _gameStateMachine.RegisterState(victoryState);

            _gameStateMachine.ChangeState<EngagementScreenState>(this);
        }

        private void Update()
        {
            _gameStateMachine.Update(this);

            if (_gameStateMachine.IsCurrentState<IngameState>() && (_previousRunStartPlaysCoroutine == null))
            {
                _timeSinceLastInput += Time.deltaTime;

                if (_nextFrames.Count > 0)
                {
                    if (_timeSinceLastInput >= MIN_TIME_BETWEEN_INPUT)
                    {
                        Vector2 playerMoveValue = _nextFrames[0];
                        _nextFrames.RemoveAt(0);
                        _runInputs[_runIndex].Add(playerMoveValue);

                        MovementComponent currentPlayer = _players[_runIndex];

                        bool didStep = PlayNextFrame();

                        if (!didStep)
                        {
                            _runInputs[_runIndex].RemoveAt(_runInputs[_runIndex].Count - 1);
                        }
                        else
                        {
                            _timeSinceLastInput = 0;

                            if (_currentLevel.HasTriggerWin(currentPlayer.transform.position))
                            {
                                DOTween.Sequence()
                                    .AppendInterval(1.0f)
                                    .AppendCallback(() => _players[_runIndex].gameObject.SetActive(false));

                                ChangeState<VictoryState>(this);
                            }
                        }
                    }
                }

                if (_runIndex < _players.Count)
                {
                    MovementComponent currentPlayer = _players[_runIndex];

                    if (currentPlayer.TryGetComponent(out DeadComponent deadComponent))
                    {
                        Camera.main.DOShakePosition(0.5f, 0.3f, 15);
                        // ChangeState<RunCompleteState>(this);
                        _previousRunStartPlaysCoroutine = StartCoroutine(DelayedNextRun()); //:TODO: should not reuse this variable or rename it
                    }
                }
            }
        }

        public void LoadLevel(LevelComponent levelPrefab)
        {
            _gameStateMachine.ChangeState<IngameState>(this);
            // _guiManager.HideLevelSelectionScreen();

            _runInputs.Clear();

            GetComponentInChildren<CameraManager>().SetTarget(null);
            // Transform transformParent = Camera.main.transform.parent;
            // transformParent.SetParent(null);
            // transformParent.localPosition = Vector3.zero;

            foreach (MovementComponent movementComponent in _players)
            {
                GameObject.Destroy(movementComponent.gameObject);
            }

            _players.Clear();

            _runIndex = -1;
            _timeStep = 0;

            if (_currentLevel != null)
            {
                GameObject.Destroy(_currentLevel.gameObject);
                _wallTiles.Clear();
            }

            _currentLevelPrefab = levelPrefab;
            _titleScreenBullshit.gameObject.SetActive(false);
            //GameObject.FindObjectOfType<Light>().shadows = LightShadows.None;
            // _currentLevel = GameObject.Instantiate(levelPrefab);
            // _currentLevel.InitialiseDependencies(this);
            // _currentLevel.InitialiseLevel();

            if (_previousRunStartPlaysCoroutine != null)
            {
                StopCoroutine(_previousRunStartPlaysCoroutine);
                _previousRunStartPlaysCoroutine = null;
            }

            _guiManager.ResetUndo();

            NextRun();
        }

        public void RestartLevel()
        {
            LoadLevel(_currentLevelPrefab);
        }

        private void ReloadLevel()
        {
            if (_currentLevel != null)
            {
                GameObject.Destroy(_currentLevel.gameObject);
                _wallTiles.Clear();
            }

            foreach (CanonComponent canonComponent in _canons)
            {
                canonComponent.Clear();
                GameObject.Destroy(canonComponent.gameObject);
            }

            _canons.Clear();

            _currentLevel = GameObject.Instantiate(_currentLevelPrefab);
            _currentLevel.InitialiseDependencies(this);
            _currentLevel.InitialiseLevel();
        }

        public void Undo()
        {
            if (_runIndex > 2)
            {
                for (int runIndex = 0; runIndex < 2; runIndex++)
                {
                    MovementComponent player = _players[_players.Count - 1];
                    _players.RemoveAt(_players.Count - 1);
                    GameObject.Destroy(player.gameObject);
                    _runInputs.RemoveAt(_runInputs.Count - 1);
                    --_runIndex;
                }
                NextRun();
            }
        }

        public void NextRun()
        {
            if (_previousRunStartPlaysCoroutine == null)
            {
                MovementComponent newPlayer = GameObject.Instantiate(_playerPrefab);

                _players.Add(newPlayer);

                ReloadLevel();

                foreach (MovementComponent movementComponent in _players)
                {
                    movementComponent.transform.position = _currentLevel.GetSpawnPosition().position;
                    movementComponent.GetComponent<PlayerStateComponent>().SetAsClone();
                }

                newPlayer.GetComponent<PlayerStateComponent>().SetAsPlayer();

                GetComponentInChildren<CameraManager>().SetTarget(newPlayer.transform);

                // Transform transformParent = Camera.main.transform.parent;
                // transformParent.SetParent(newPlayer.transform, false);
                // transformParent.localPosition = Vector3.zero;

                _runInputs.Add(new List<Vector2>());
                ++_runIndex;
                _timeStep = 0;

                _previousRunStartPlaysCoroutine = StartCoroutine(PlayPreviousRunStartsCoroutine());
            }
        }

        private IEnumerator PlayPreviousRunStartsCoroutine()
        {
            while (_timeStep < _runIndex)
            {
                _guiManager.DisplayRunNumber(_runIndex - _timeStep);
                yield return new WaitForSeconds(1.0f);
                PlayNextFrame();
            }
            _guiManager.HideRunNumber();
            _previousRunStartPlaysCoroutine = null;
        }

        private bool PlayNextFrame()
        {
            bool doesStep = true;

            Dictionary<int, Vector2> movements = new Dictionary<int, Vector2>();

            for (int clonePlayerIndex = _runIndex; clonePlayerIndex >= 0; clonePlayerIndex--)
            {
                int delayedTimeStep = _timeStep - clonePlayerIndex;

                if ((delayedTimeStep >= 0) && doesStep)
                {
                    MovementComponent clonePlayer = _players[clonePlayerIndex];

                    if (delayedTimeStep < _runInputs[clonePlayerIndex].Count)
                    {
                        Vector2 cloneMoveValue = _runInputs[clonePlayerIndex][delayedTimeStep];

                        bool canMove = MovementComponent.CanMove(clonePlayer, cloneMoveValue, _players, _wallTiles, _runInputs, _timeStep);
                        bool hasTriggerSomething = TriggerBlock(clonePlayer, cloneMoveValue);

                        if (canMove)
                        {
                            movements.Add(clonePlayerIndex, cloneMoveValue);
                        }
                        else if (!hasTriggerSomething && (clonePlayerIndex == _runIndex))
                        {
                            doesStep = false;
                        }
                    }
                }
            }

            if (doesStep)
            {
                foreach (CanonComponent canonComponent in _canons)
                {
                    canonComponent.Step(movements);
                }

                foreach (KeyValuePair<int,Vector2> movement in movements)
                {
                    MovementComponent clonePlayer = _players[movement.Key];
                    clonePlayer.Move(movement.Value);
                }

                PlayRhythmSound();

                ++_timeStep;
            }

            return doesStep;
        }

        private void PlayRhythmSound()
        {
            _playerMoveSound.gameObject.SetActive(false);
            _playerMoveSound.gameObject.SetActive(true);
        }

        public void MoveTriggered(Vector2 playerMoveValue)
        {
            if ((_previousRunStartPlaysCoroutine == null) && (IsCurrentState<IngameState>()))
            {
                _nextFrames.Add(playerMoveValue);
            }
        }

        private IEnumerator DelayedNextRun()
        {
            _guiManager.SetIngameMessage("You are dead");
            PlayRhythmSound();
            yield return new WaitForSeconds(1.0f);
            _guiManager.SetIngameMessage("Prepare for next run");
            PlayRhythmSound();
            yield return new WaitForSeconds(1.0f);
            _guiManager.SetIngameMessage("But you are not alone");
            PlayRhythmSound();


            yield return new WaitForSeconds(1.0f);
            _previousRunStartPlaysCoroutine = null;

            NextRun();
        }

        private bool IsCurrentState<TYPE_STATE>()
        {
            return _gameStateMachine.IsCurrentState<TYPE_STATE>();
        }

        private bool TriggerBlock(MovementComponent currentPlayer, Vector2 playerMoveValue)
        {
            Vector3 currentPlayerPosition = currentPlayer.transform.position;
            Vector3 predictedNextPlayerPosition = Utilities.NextPosition(currentPlayerPosition, playerMoveValue);

            return _currentLevel.Trigger(currentPlayer, predictedNextPlayerPosition);
        }

        public void AddWallTile(Vector3 wallTilePosition)
        {
            _wallTiles.Add(wallTilePosition);
        }

        public void RemoveWallTile(Vector3 wallTilePosition)
        {
            _wallTiles.Remove(wallTilePosition);
        }

        public void ChangeState<TYPE_STATE>(GameManager owner) where TYPE_STATE : State<GameManager>, new()
        {
            _gameStateMachine.ChangeState<TYPE_STATE>(owner);
        }

        public int GetTimeStep()
        {
            return _timeStep;
        }

        public void RegisterCanon(CanonComponent canonComponent)
        {
            _canons.Add(canonComponent);
        }

        public bool DoesCollideWithWall(Vector3 position)
        {
            bool doesCollideWithWall = false;

            foreach (Vector3 occupiedTilePosition in _wallTiles)
            {
                if (Utilities.ArePositionsEqual(position, occupiedTilePosition))
                {
                    doesCollideWithWall = true;
                }
            }

            return doesCollideWithWall;
        }

        public PlayerStateComponent DoesCollideWithPlayer(Vector3 currentBulletPosition, Vector3 nextBulletPosition, Dictionary<int, Vector2> futurePlayersMovements)
        {
            PlayerStateComponent collidingPlayer = null;

            for (int playerIndex = 0; playerIndex < _players.Count; playerIndex++)
            {
                MovementComponent player = _players[playerIndex];
                Vector3 currentPlayerPosition = player.transform.position;
                Vector3 nextPlayerPosition = currentPlayerPosition;

                if (futurePlayersMovements.ContainsKey(playerIndex))
                {
                    nextPlayerPosition = Utilities.NextPosition(currentPlayerPosition, futurePlayersMovements[playerIndex]);
                }

                if (Utilities.ArePositionsEqual(nextPlayerPosition, nextBulletPosition))
                {
                    collidingPlayer = player.GetComponent<PlayerStateComponent>();
                }
                else if (Utilities.ArePositionsEqual(currentPlayerPosition, nextBulletPosition) && Utilities.ArePositionsEqual(nextPlayerPosition, currentBulletPosition))
                {
                    //Check if exchanged positions
                    collidingPlayer = player.GetComponent<PlayerStateComponent>();
                }
            }

            return collidingPlayer;
        }

        public void SetCurrentLevelAsCompleted()
        {
            int currentScore = _timeStep;
            int bestScore = PlayerPrefs.GetInt(_currentLevelPrefab.name, 0);

            if((bestScore == 0) || (currentScore < bestScore))
            {
                PlayerPrefs.SetInt(_currentLevelPrefab.name, currentScore);
                PlayerPrefs.Save();
            }
        }

        public int GetCurrentLevelBestScore()
        {
            int bestScore = PlayerPrefs.GetInt(_currentLevelPrefab.name, 0);

            return bestScore;
        }

        public string GetCurrentLevelName()
        {
            return _currentLevelPrefab.name;
        }
    }
}