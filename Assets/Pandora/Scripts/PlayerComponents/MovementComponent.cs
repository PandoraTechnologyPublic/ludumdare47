using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pandora
{
    public class MovementComponent : MonoBehaviour
    {
        private Animator anim;
        [SerializeField]
        private Transform root;
        private void Start()
        {
            anim = this.GetComponentInChildren<Animator>();
        }

        public void Move(Vector2 movementVector)
        {
            transform.position = Utilities.NextPosition(transform.position, movementVector);
            root.forward = new Vector3(movementVector.x, 0, movementVector.y);
            anim.SetTrigger("goForth");
            // if (Math.Abs(movementVector.y) >= 0.5f)
            // {
            //     transform.Translate(Vector3.forward * Math.Sign(movementVector.y));
            // }
            // else if (Math.Abs(movementVector.x) >= 0.5f)
            // {
            //     transform.Translate(Vector3.right * Math.Sign(movementVector.x));
            // }
        }

        public static bool CanMove(MovementComponent currentPlayer, Vector2 moveValue, List<MovementComponent> players, List<Vector3> occupiedTilePositions, List<List<Vector2>> runInputs, int timeStep)
        {
            bool canMove = true;
            Vector3 currentPlayerPosition = currentPlayer.transform.position;
            Vector3 predictedNextPlayerPosition = Utilities.NextPosition(currentPlayerPosition, moveValue);

            for (int clonePlayerIndex = 0; clonePlayerIndex < players.Count; clonePlayerIndex++)
            {
                MovementComponent movementComponent = players[clonePlayerIndex];

                if (movementComponent != currentPlayer)
                {
                    int delayedTimeStep = timeStep - clonePlayerIndex;
                    Vector3 currentClonePosition = movementComponent.transform.position;

                    if ((delayedTimeStep >= 0) && (delayedTimeStep < runInputs[clonePlayerIndex].Count))
                    {
                        Vector2 nextMove = runInputs[clonePlayerIndex][delayedTimeStep];
                        Vector3 predictedNextClonePlayerPosition = Utilities.NextPosition(currentClonePosition, nextMove);

                        if (Utilities.ArePositionsEqual(predictedNextPlayerPosition, predictedNextClonePlayerPosition))
                        {
                            canMove = false;
                        }
                        else if (Utilities.ArePositionsEqual(currentPlayerPosition, predictedNextClonePlayerPosition) && Utilities.ArePositionsEqual(predictedNextPlayerPosition, currentClonePosition))
                        {
                            // avoid that player will not exchange position with clone
                            canMove = false;
                        }
                    }
                    else
                    {
                        if (Utilities.ArePositionsEqual(predictedNextPlayerPosition, currentClonePosition))
                        {
                            canMove = false;
                        }
                    }
                }
            }

            if (canMove)
            {
                foreach (Vector3 occupiedTilePosition in occupiedTilePositions)
                {
                    if (Utilities.ArePositionsEqual(predictedNextPlayerPosition, occupiedTilePosition))
                    {
                        canMove = false;
                    }
                }
            }

            return canMove;
        }
    }
}