using UnityEngine;

namespace Pandora
{
    public class PlayerStateComponent : MonoBehaviour
    {
        [SerializeField] private Material _playerMaterial;
        [SerializeField] private Material _playerCloneMaterial;
        [SerializeField] private Material _playerFreezedMaterial;

        public void Kill()
        {
            gameObject.AddComponent<DeadComponent>();
            GetComponentInChildren<SkinnedMeshRenderer>().material = _playerFreezedMaterial;
            GetComponentInChildren<Animator>().SetBool("dead", true);
        }

        public void SetAsClone()
        {
            GetComponentInChildren<SkinnedMeshRenderer>().material = _playerCloneMaterial;
            GetComponentInChildren<Animator>().SetBool("dead", false);
        }

        public void SetAsPlayer()
        {
            GetComponentInChildren<SkinnedMeshRenderer>().material = _playerMaterial;
        }
    }
}