using System;
using UnityEngine;

namespace Pandora
{
    public static class Utilities
    {
        public static Vector3 NextPosition(Vector3 position, Vector2 moveValue)
        {
            Vector3 nextPosition = position;

            if (Math.Abs(moveValue.y) >= 0.5f)
            {
                nextPosition.z += Math.Sign(moveValue.y);
            }
            else if (Math.Abs(moveValue.x) >= 0.5f)
            {
                nextPosition.x += Math.Sign(moveValue.x);
            }

            return nextPosition;
        }

        public static bool ArePositionsEqual(Vector3 firstPosition, Vector3 secondPosition)
        {
            return Vector3.Distance(firstPosition, secondPosition) < 0.1f;
        }
    }
}