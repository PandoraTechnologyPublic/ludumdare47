using System;
using System.Collections.Generic;

namespace Pandora.States
{
    public class StateMachine<TYPE_OWNER>
    {
        private State<TYPE_OWNER> _currentState;
        private readonly Dictionary<Type, State<TYPE_OWNER>> _states = new Dictionary<Type, State<TYPE_OWNER>>();

        public void RegisterState(State<TYPE_OWNER> state)
        {
            _states.Add(state.GetType(), state);
        }

        public void Update(TYPE_OWNER owner)
        {
            _currentState.Update(owner);
        }

        public void ChangeState<TYPE_STATE>(TYPE_OWNER owner) where TYPE_STATE : State<TYPE_OWNER>, new()
        {
            _currentState?.Exit(owner);

            _currentState = _states[typeof(TYPE_STATE)];

            _currentState.Enter(owner);
        }

        public bool IsCurrentState<TYPE_STATE>()
        {
            return typeof(TYPE_STATE) == _currentState.GetType();
        }
    }
}