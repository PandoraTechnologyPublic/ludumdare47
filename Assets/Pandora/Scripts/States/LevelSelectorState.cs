namespace Pandora.States
{
    public class LevelSelectorState : State<GameManager>
    {
        private GuiManager _guiManager;

        public void InitialiseDependencies(GuiManager guiManager)
        {
            _guiManager = guiManager;
        }

        public override void Enter(GameManager owner)
        {
            _guiManager.ShowLevelSelectionScreen();
        }

        public override void Update(GameManager owner)
        {

        }

        public override void Exit(GameManager owner)
        {
            _guiManager.HideLevelSelectionScreen();
        }
    }
}