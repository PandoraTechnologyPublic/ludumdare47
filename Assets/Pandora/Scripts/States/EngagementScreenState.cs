using UnityEngine;
using UnityEngine.InputSystem;

namespace Pandora.States
{
    public class EngagementScreenState : State<GameManager>
    {
        private PlayerInput _playerInput;
        private GuiManager _guiManager;

        public void InitialiseDependencies(GuiManager guiManager, PlayerInput playerInput)
        {
            _guiManager = guiManager;
            _playerInput = playerInput;
        }

        public override void Enter(GameManager owner)
        {
            _guiManager.ShowEngagementScreen();
        }

        public override void Update(GameManager owner)
        {
            if (_playerInput.actions["Player/Fire"].triggered)
            {
                owner.ChangeState<LevelSelectorState>(owner);
            }
        }

        public override void Exit(GameManager owner)
        {
            _guiManager.HideEngagementScreen();
        }
    }
}