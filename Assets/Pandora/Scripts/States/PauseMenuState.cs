using UnityEngine;
using UnityEngine.InputSystem;

namespace Pandora.States
{
    public class PauseMenuState : State<GameManager>
    {
        private PlayerInput _playerInput;
        private GuiManager _guiManager;

        public void InitialiseDependencies(GuiManager guiManager, PlayerInput playerInput)
        {
            _guiManager = guiManager;
            _playerInput = playerInput;
        }

        public override void Enter(GameManager owner)
        {
            _guiManager.ShowPauseScreen();
            Time.timeScale = 0;
        }

        public override void Update(GameManager owner)
        {
            if (_playerInput.actions["Player/Pause"].triggered)
            {
                owner.ChangeState<IngameState>(owner);
            }
        }

        public override void Exit(GameManager owner)
        {
            _guiManager.HidePauseScreen();
            Time.timeScale = 1;
        }
    }
}