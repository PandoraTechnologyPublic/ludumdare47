using UnityEngine.InputSystem;

namespace Pandora.States
{
    public class IngameState : State<GameManager>
    {
        private PlayerInput _playerInput;
        private GuiManager _guiManager;

        public void InitialiseDependencies(GuiManager guiManager, PlayerInput playerInput)
        {
            _guiManager = guiManager;
            _playerInput = playerInput;
        }

        public override void Enter(GameManager owner)
        {
            _guiManager.ShowIngameScreen();
        }

        public override void Update(GameManager owner)
        {
            if (_playerInput.actions["Player/Pause"].triggered)
            {
                owner.ChangeState<PauseMenuState>(owner);
            }
        }

        public override void Exit(GameManager owner)
        {
            _guiManager.HideIngameScreen();
        }
    }
}