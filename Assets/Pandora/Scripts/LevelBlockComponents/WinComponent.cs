using System;
using UnityEngine;

namespace Pandora
{
    public class WinComponent : MonoBehaviour
    {
        [SerializeField] private Animator _warpGateAnimator;
        [SerializeField] private AudioSource _warpSound;

        private static readonly int WARP = Animator.StringToHash("Warp");

        private void Start()
        {
            _warpSound.gameObject.SetActive(false);
        }

        public void Activate()
        {
            _warpGateAnimator.SetTrigger(WARP);
            _warpSound.gameObject.SetActive(false);
            _warpSound.gameObject.SetActive(true);
        }
    }
}