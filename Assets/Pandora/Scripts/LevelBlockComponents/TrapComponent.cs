using System;
using UnityEngine;

namespace Pandora
{
    public class TrapComponent : MonoBehaviour
    {
        [SerializeField] private Animator _spikeAnimator;
        [SerializeField] private AudioSource _spikeSound;

        private static readonly int SPIKE_OUT = Animator.StringToHash("SpikeOut");

        private void Start()
        {
            _spikeSound.gameObject.SetActive(false);
        }

        public void Activate()
        {
            if (_spikeAnimator != null)
            {
                _spikeAnimator.SetTrigger(SPIKE_OUT);
            }

            _spikeSound.gameObject.SetActive(false);
            _spikeSound.gameObject.SetActive(true);
        }
    }
}