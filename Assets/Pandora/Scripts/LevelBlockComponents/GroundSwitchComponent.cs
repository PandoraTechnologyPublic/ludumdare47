using System.Collections.Generic;
using UnityEngine;

namespace Pandora.LevelBlockComponents
{
    public class GroundSwitchComponent : MonoBehaviour
    {
        [SerializeField] private Animator _buttonAnimator;
        [SerializeField] private bool _doesKillThePlayer;
        [SerializeField] private int _deactivationTurnCount = 0;
        [SerializeField] private List<DoorComponent> _doors;

        private GameManager _gameManager;
        private int _activationStepIndex;

        private static readonly int PRESS = Animator.StringToHash("Press");
        private static readonly int RELEASE = Animator.StringToHash("Release");

        public void InitialiseDependencies(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        public void Activate(GameManager gameManager, MovementComponent currentPlayer)
        {
            _buttonAnimator.SetTrigger(PRESS);
            _activationStepIndex = gameManager.GetTimeStep();

            foreach (DoorComponent door in _doors)
            {
                door.Open(gameManager);
            }

            if (_doesKillThePlayer)
            {
                currentPlayer.GetComponent<PlayerStateComponent>().Kill();
            }
        }

        public void Deactivate(GameManager gameManager)
        {
            _buttonAnimator.SetTrigger(RELEASE);
            _activationStepIndex = gameManager.GetTimeStep();

            foreach (DoorComponent door in _doors)
            {
                door.Close(gameManager);
            }
        }

        private void Update()
        {
            if ((_deactivationTurnCount > 0) && (_activationStepIndex > 0))
            {
                int timeStep = _gameManager.GetTimeStep();

                if (timeStep >= _activationStepIndex + _deactivationTurnCount)
                {
                    Deactivate(_gameManager);
                }
            }
        }
    }
}