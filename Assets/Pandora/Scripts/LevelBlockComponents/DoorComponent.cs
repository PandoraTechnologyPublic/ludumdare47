using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pandora.LevelBlockComponents
{
    public class DoorComponent : MonoBehaviour
    {
        [SerializeField] private Animator _doorAnimator;
        [SerializeField] private AudioSource _doorSound;
        [SerializeField] private List<Transform> _collisionBlocks;

        private static readonly int OPEN = Animator.StringToHash("Open");
        private static readonly int CLOSE = Animator.StringToHash("Close");

        private void Start()
        {
            _doorSound.gameObject.SetActive(false);
        }

        public void Open(GameManager gameManager)
        {
            foreach (Transform collisionBlock in _collisionBlocks)
            {
                Vector3 doorPosition = collisionBlock.position;

                doorPosition.y = 0;
                gameManager.RemoveWallTile(doorPosition);
                collisionBlock.gameObject.SetActive(false);
            }

            _doorSound.gameObject.SetActive(false);
            _doorSound.gameObject.SetActive(true);

            _doorAnimator.SetTrigger(OPEN);
        }

        public void Close(GameManager gameManager)
        {
            foreach (Transform collisionBlock in _collisionBlocks)
            {
                Vector3 doorPosition = collisionBlock.position;

                doorPosition.y = 0;
                gameManager.AddWallTile(doorPosition);
                collisionBlock.gameObject.SetActive(true);
            }

            _doorSound.gameObject.SetActive(false);
            _doorSound.gameObject.SetActive(true);

            _doorAnimator.SetTrigger(CLOSE);
        }
    }
}