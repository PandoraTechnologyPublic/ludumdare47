using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pandora.LevelBlockComponents
{
    public class HoleComponent : MonoBehaviour
    {
        [SerializeField] private int _holeDepth;
        [SerializeField] private AudioSource _fallSound;

        private void Start()
        {
            _fallSound.gameObject.SetActive(false);
        }

        public int GetHoleDepth()
        {
            return _holeDepth;
        }

        public void DecreaseHoleDepth()
        {
            Assert.IsTrue(_holeDepth > 0);
            _holeDepth--;

            _fallSound.gameObject.SetActive(false);
            _fallSound.gameObject.SetActive(true);
        }
    }
}