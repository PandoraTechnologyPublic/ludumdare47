using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pandora.LevelBlockComponents
{
    public class CanonComponent : MonoBehaviour
    {
        [SerializeField] private int _fireRate = 5;
        [SerializeField] private Transform _shootingNode;
        [SerializeField] private GameObject _bulletPrefab;
        [SerializeField] private Animator _canonAnimator;
        [SerializeField] private AudioSource _canonSound;
        [SerializeField] private AudioSource _hitSound;

        private readonly List<GameObject> _bullets = new List<GameObject>();
        private int _lastShootTick;

        private GameManager _gameManager;

        public void InitialiseDependencies(GameManager gameManager)
        {
            _gameManager = gameManager;
            _gameManager.RegisterCanon(this);
        }

        private void Start()
        {
            _canonSound.gameObject.SetActive(false);
            _hitSound.gameObject.SetActive(false);
        }

        public void Step(Dictionary<int, Vector2> futurePlayersMovements)
        {
            _lastShootTick--;

            // :TODO: step all bullet
            for (int bulletIndex = _bullets.Count - 1; bulletIndex >= 0; bulletIndex--)
            {
                GameObject bullet = _bullets[bulletIndex];
                Vector3 bulletPosition = bullet.transform.position;
                //:TODO: Free previous tile
                // _gameManager.UnregisterOccupiedTile(bulletPosition);

                MoveBullet(bulletPosition, bullet, futurePlayersMovements);

                // :TODO: if bullet hit a wall/player it disappear
            }

            if (_lastShootTick <= 0)
            {
                GameObject bullet = GameObject.Instantiate(_bulletPrefab);
                _bullets.Add(bullet);
                Vector3 bulletPosition = transform.position;
                bullet.transform.forward = transform.forward;
                bulletPosition.y = 0;
                _canonSound.gameObject.SetActive(false);
                _canonSound.gameObject.SetActive(true);
                MoveBullet(bulletPosition, bullet, futurePlayersMovements);
                _lastShootTick = _fireRate;

                _canonAnimator.SetTrigger("Shoot");
            }
        }

        private void MoveBullet(Vector3 bulletPosition, GameObject bullet, Dictionary<int, Vector2> futurePlayersMovements)
        {
            Vector3 shootDirection = _shootingNode.forward;

            Assert.IsTrue((Math.Abs(Mathf.Abs(shootDirection.x) - 1) < 0.1f) || (Math.Abs(Mathf.Abs(shootDirection.z) - 1) < 0.1f));

            Vector2 moveValue = new Vector2(shootDirection.x, shootDirection.z);
            Vector3 nextPosition = Utilities.NextPosition(bulletPosition, moveValue);

            if (_gameManager.DoesCollideWithWall(nextPosition))
            {
                //:TODO: Bullet check if collide with other bullet -> use collision
                //:TODO: and if exchanged positions
                KillBullet(bullet);
            }
            else
            {
                PlayerStateComponent collidedPlayer = _gameManager.DoesCollideWithPlayer(bulletPosition, nextPosition, futurePlayersMovements);

                if (collidedPlayer != null)
                {
                    collidedPlayer.Kill();
                    KillBullet(bullet);
                }
                else
                {

                    bulletPosition += shootDirection;

                    bullet.transform.position = bulletPosition;

                    // _gameManager.RegisterOccupiedTile(bulletPosition);
                }
            }
        }

        private void KillBullet(GameObject bullet)
        {
            _bullets.Remove(bullet);
            GameObject.Destroy(bullet.gameObject);
            _hitSound.gameObject.SetActive(false);
            _hitSound.gameObject.SetActive(true);
        }

        public void Clear()
        {
            foreach (GameObject bullet in _bullets)
            {
                GameObject.Destroy(bullet.gameObject);
            }
        }
    }
}