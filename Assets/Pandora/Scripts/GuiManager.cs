using System;
using Pandora.Ui;
using UnityEngine;

namespace Pandora
{
    public class GuiManager : MonoBehaviour
    {
        [SerializeField] private RectTransform _engagementScreen;
        [SerializeField] private IngameScreenLayout _ingameScreen;
        [SerializeField] private PauseScreenLayout _pauseScreen;
        [SerializeField] private LevelSelectionScreenLayout _levelSelectionScreen;
        [SerializeField] private RectTransform _gameOverScreen;

        private GameManager _gameManager;

        public void InitialiseDependencies(GameManager gameManager, LevelManager levelManager)
        {
            _gameManager = gameManager;
            _ingameScreen.InitialiseDependencies(gameManager);
            _pauseScreen.InitialiseDependencies(gameManager);
            _levelSelectionScreen.InitialiseDependencies(levelManager);
        }

        private void Start()
        {
            _ingameScreen.gameObject.SetActive(false);
            _levelSelectionScreen.gameObject.SetActive(false);
            _pauseScreen.gameObject.SetActive(false);
            _gameOverScreen.gameObject.SetActive(false);
        }

        public void ShowLevelSelectionScreen()
        {
            _levelSelectionScreen.UpdateData();
            _levelSelectionScreen.gameObject.SetActive(true);
        }

        public void HideLevelSelectionScreen()
        {
            _levelSelectionScreen.gameObject.SetActive(false);
        }

        public void ShowEngagementScreen()
        {
            _engagementScreen.gameObject.SetActive(true);
        }

        public void HideEngagementScreen()
        {
            _engagementScreen.gameObject.SetActive(false);
        }

        public void ShowPauseScreen()
        {
            _pauseScreen.gameObject.SetActive(true);
        }

        public void HidePauseScreen()
        {
            _pauseScreen.gameObject.SetActive(false);
        }

        public void ShowIngameScreen()
        {
            _ingameScreen.gameObject.SetActive(true);
        }

        public void HideIngameScreen()
        {
            _ingameScreen.gameObject.SetActive(false);
        }

        public void ShowVictoryScreen()
        {
            _gameOverScreen.gameObject.SetActive(true);
        }

        public void HideVictoryScreen()
        {
            _gameOverScreen.gameObject.SetActive(false);
        }

        public void DisplayRunNumber(int timeStep)
        {
            _ingameScreen.SetRunNumber(timeStep);
        }

        public void HideRunNumber()
        {
            _ingameScreen.HideRunNumber();
        }

        public void SetIngameMessage(string message)
        {
            _ingameScreen.SetMessage(message);
        }

        public void ResetUndo()
        {
            _ingameScreen.ResetUndo();
        }
    }
}