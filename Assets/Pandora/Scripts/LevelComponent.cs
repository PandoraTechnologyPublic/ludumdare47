using System;
using System.Collections.Generic;
using Pandora.LevelBlockComponents;
using UnityEngine;

namespace Pandora
{
    public class LevelComponent : MonoBehaviour
    {
        [SerializeField] private Transform _spawnPosition;

        private GameManager _gameManager;

        public void InitialiseDependencies(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        public void InitialiseLevel()
        {
            Transform[] children = GetComponentsInChildren<Transform>();

            foreach (Transform child in children)
            {
                if (child.TryGetComponent(out BoxCollider boxCollider))
                {
                    Vector3 tilePosition = child.position;

                    tilePosition.y = 0;

                    _gameManager.AddWallTile(tilePosition);
                }

                if (child.TryGetComponent(out WallSwitchComponent wallSwitchComponent))
                {
                    wallSwitchComponent.InitialiseDependencies(_gameManager);
                }

                if (child.TryGetComponent(out GroundSwitchComponent groundSwitchComponent))
                {
                    groundSwitchComponent.InitialiseDependencies(_gameManager);
                }

                if (child.TryGetComponent(out CanonComponent canonComponent))
                {
                    canonComponent.InitialiseDependencies(_gameManager);
                }
            }
        }

        public Transform GetSpawnPosition()
        {
            return _spawnPosition;
        }

        public bool Trigger(MovementComponent currentPlayer, Vector3 predictedNextPlayerPosition)
        {
            bool hasTrigger = false;
            Transform[] children = GetComponentsInChildren<Transform>();

            foreach (Transform child in children)
            {
                Vector3 tilePosition = child.position;

                tilePosition.y = 0;

                if (Utilities.ArePositionsEqual(predictedNextPlayerPosition, tilePosition))
                {
                    if (child.TryGetComponent(out WallSwitchComponent wallSwitchComponent))
                    {
                        wallSwitchComponent.Activate(_gameManager, currentPlayer);
                        hasTrigger = true;
                    }

                    if (child.TryGetComponent(out TrapComponent trapComponent))
                    {
                        trapComponent.Activate();
                        currentPlayer.GetComponent<PlayerStateComponent>().Kill();
                    }

                    if (child.TryGetComponent(out GroundSwitchComponent groundSwitchComponent))
                    {
                        groundSwitchComponent.Activate(_gameManager, currentPlayer);
                    }

                    if (child.TryGetComponent(out HoleComponent holeComponent))
                    {
                        if (holeComponent.GetHoleDepth() > 0)
                        {
                            Vector3 position = currentPlayer.transform.position;

                            position.y -= holeComponent.GetHoleDepth();

                            currentPlayer.transform.position = position;

                            if (holeComponent.GetHoleDepth() == 0)
                            {
                                GameObject.Destroy(trapComponent);
                            }
                            else
                            {
                                holeComponent.DecreaseHoleDepth();
                            }

                            currentPlayer.GetComponent<PlayerStateComponent>().Kill();
                            //_gameManager.UnregisterOccupiedTile(currentPlayer.);
                        }
                    }
                }
            }

            return hasTrigger;
        }

        public bool HasTriggerWin(Vector3 playerPosition)
        {
            bool hasTriggeredWin = false;
            Transform[] children = GetComponentsInChildren<Transform>();

            foreach (Transform child in children)
            {
                Vector3 tilePosition = child.position;

                tilePosition.y = 0;

                if (Utilities.ArePositionsEqual(playerPosition, tilePosition))
                {
                    if (child.TryGetComponent(out WinComponent winComponent))
                    {
                        hasTriggeredWin = true;
                        winComponent.Activate();
                    }
                }
            }

            return hasTriggeredWin;
        }
    }
}